#include "set.h"
#include "../common/Common.h"
//
struct Node {
   Hechizo elem; // el elemento que este nodo almacena
   Node* next; // siguiente nodo de la cadena de punteros
};

struct SetSt {
   int size; // cantidad de elementos del conjunto
   Node* first; // puntero al primer elemento
};

/**
  Invariantes de representacion:
    - size es la cantidad de nodos
    - no hay nodos con hechizos repetidos
**/

/// Proposito: retorna un conjunto de hechizos vacio
/// Costo: O(1)
Set emptyS() {
   SetSt* s = new SetSt;
   s -> size = 0;
   s -> first = NULL;
   return s;

}

/// Proposito: retorna la cantidad de hechizos
/// Costo: O(1)
int sizeS(Set s) {
   return s -> size;
}

/// Proposito: indica si el hechizo pertenece al conjunto
/// Costo: O(h), h = cantidad de hechizos
bool belongsS(Hechizo h, Set s) {
   bool res = false;
   Node* primero = s->first;
   while (primero != NULL){
    res = res | (mismoHechizo (h , primero -> elem));
    primero = primero -> next;
   }

   return res;
}


void addS(Hechizo h, Set s) {
   if(!(belongsS(h,s))){
   s -> size++ ;
   Node* hechizoNuevo = new Node;
   hechizoNuevo -> elem = h;
   hechizoNuevo -> next = s -> first;
   s -> first = hechizoNuevo;
    }
}


/// Proposito: borra un hechizo del conjunto (si no existe no hace nada)
/// Costo: O(h), h = cantidad de hechizos
void removeS(Hechizo h, Set s) {
   Node* actual = s->first;
   Node* anterior = NULL;
   if(belongsS(h,s)){
      while (!(mismoHechizo(actual -> elem,h))){
      anterior = actual;
      actual = actual -> next;
      }
      if (anterior == NULL){
      s -> size = s-> size -1;
      s -> first = s -> first -> next;
      delete actual;
      }
      else{
      anterior -> next = actual -> next;
      s -> size = s-> size -1;
      delete actual;
      }
   }
}

/// Proposito: borra toda la memoria consumida por el conjunto (pero no la de los hechizos)
/// Costo: O(n)
void destroyS(Set s) {
   Node* puntero = s -> first ;
   while (puntero != NULL){
      s -> first = s -> first -> next;
      delete (puntero);
      puntero = s->first;
   }
   delete (s);
}

/// Proposito: retorna un nuevo conjunto que es la union entre ambos (no modifica estos conjuntos)
/// Costo: O(h^2), h = cantidad de hechizos
Set unionS(Set s1, Set s2) {
   SetSt* nuevoSet1 = emptyS();
   Node* puntero = s1 -> first;
   while (puntero != NULL){
      addS(puntero -> elem,nuevoSet1);
      puntero = puntero -> next;
   }

   puntero = s2 -> first;
   while (puntero != NULL){
      addS(puntero -> elem,nuevoSet1);
      puntero = puntero -> next;
   }

   return nuevoSet1;
}
