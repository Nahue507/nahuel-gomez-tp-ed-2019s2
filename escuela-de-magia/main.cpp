
#include <iostream>
#include <cstdlib>
#include <vector>
#include "../hechizo/hechizo.h"
#include "../mago/mago.h"
#include "../set/set.h"
#include "../map/map.h"
#include "../maxheap/maxheap.h"
#include "escuela_magia.h"
#include "test_escuela_magia.h"
#include "../common/Common.h"






using namespace std;

/// Proposito: Retorna todos los hechizos aprendidos por los magos.
/// Eficiencia: O(h^2+(log m)) (h = cantidad de hechizos, m = cantidad de magos)

Set hechizosAprendidos(EscuelaDeMagia m) {
   SetSt* hechizos = emptyS();
   while(!estaVacia(m)){
    hechizos = unionS(hechizos , hechizosDe(nombreMago(unEgresado(m)),m));
    quitarEgresado(m);
   }
   return hechizos;
}


/// Proposito: Dado el nombre de un mago indica si dicho mago es un experto
/// Eficiencia: O(log m)
bool esUnExperto(string mago, EscuelaDeMagia m){
    return leFaltanAprender(mago,m) == 0;
}

/// Proposito: Indica si existe un mago que sabe todos los hechizos ense�ados por la escuela.
/// Eficiencia: O(m log m)

bool hayUnExperto(EscuelaDeMagia m) {
   bool res = false;
   for (std::size_t i = 0; i < magos(m).size(); i++) {
        string mag = magos(m)[i];
        res = res || esUnExperto(mag,m);
   }
   return res;
}



/// Proposito: Devuelve una maxheap con los magos que saben todos los hechizos dados por la escuela, quit�ndolos de la escuela.
/// Eficiencia: m log m siendo m la cantidad de magos expertos.
MaxHeap egresarExpertos(EscuelaDeMagia m) {
   MaxHeap expertos = emptyH();
    while(hayUnExperto(m)){
        insertH(unEgresado(m),expertos);
        quitarEgresado(m);
    }
    return expertos;
   }






int main()
{
   correrTestEscuelaMagia();
   return 0;
}


