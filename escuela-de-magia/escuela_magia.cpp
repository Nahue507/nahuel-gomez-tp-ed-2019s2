#include "escuela_magia.h"
#include "../common/Common.h"

struct EscuelaDeMagiaSt {
    SetSt* hechizos;
    MapSt* alumnos;
    MaxHeapSt* alumnosHeap;
};

/// Prop�sito: Devuelve una escuela vac�a.
/// O(1)
EscuelaDeMagia fundarEscuela() {
   EscuelaDeMagia esc = new EscuelaDeMagiaSt;
   esc -> hechizos = emptyS();
   esc -> alumnos = emptyM();
   esc -> alumnosHeap = emptyH();

   return esc;


}

/// Prop�sito: Indica si la escuela est� vac�a.
/// O(1)
bool estaVacia(EscuelaDeMagia m) {
   return isEmptyH(m -> alumnosHeap);
}

/// Prop�sito: Incorpora un mago a la escuela (si ya existe no hace nada).
/// O(log m)
void registrar(string nombre, EscuelaDeMagia m) {
   if(NULL == lookupM(nombre,m->alumnos)){
    MagoSt* nuevoMago = crearMago(nombre);
    assocM(nombre,  nuevoMago, m->alumnos);
    insertH(nuevoMago,m->alumnosHeap);

   }
}

/// Prop�sito: Devuelve los nombres de los magos registrados en la escuela.
/// O(m)
vector<string> magos(EscuelaDeMagia m) {
   return domM(m->alumnos);
}

/// Prop�sito: Devuelve los hechizos que conoce un mago dado.
/// Precondici�n: Existe un mago con dicho nombre.
/// O(log m)
Set hechizosDe(string nombre, EscuelaDeMagia m) {
   return hechizosMago(lookupM(nombre, m->alumnos));
}

/// Prop�sito: Dado un mago, indica la cantidad de hechizos que la escuela ha dado y �l no sabe.
/// Precondici�n: Existe un mago con dicho nombre.
/// O(log m)
int leFaltanAprender(string nombre, EscuelaDeMagia m) {
   return sizeS(m->hechizos) - sizeS(hechizosDe(nombre,m));
}

/// Prop�sito: Devuelve el mago que m�s hechizos sabe.
/// Precondici�n: La escuela no est� vac�a.
/// O(log m)
Mago unEgresado(EscuelaDeMagia m) {
   return maxH(m->alumnosHeap);
}

/// Prop�sito: Devuelve la escuela sin el mago que m�s sabe.
/// Precondici�n: La escuela no est� vac�a.
/// O(log m)
void quitarEgresado(EscuelaDeMagia m) {
    deleteM(nombreMago(maxH(m->alumnosHeap)),m->alumnos);
    deleteMax (m->alumnosHeap);


}

/// Prop�sito: Ense�a un hechizo a un mago existente, y si el hechizo no existe en la escuela es incorporado a la misma.
/// O(m . log m + log h)
void enseniar(Hechizo h, string nombre, EscuelaDeMagia m) {
   addS(h ,m -> hechizos);
   MaxHeapSt* nuevaHeap = emptyH();
   aprenderHechizo(h, lookupM(nombre, m->alumnos));
   while(!(sizeH(m->alumnosHeap) == 0)){
    insertH(maxH(m->alumnosHeap),nuevaHeap);
    deleteMax(m->alumnosHeap);
   }
   destroyH(m->alumnosHeap);
   m->alumnosHeap = nuevaHeap;
}

/// Prop�sito: Libera toda la memoria creada por la escuela (incluye magos, pero no hechizos).
void destruirEscuela(EscuelaDeMagia m) {
   Mago masP = maxH(m->alumnosHeap);
   while (! (masP == NULL)){
    destroyMago(masP);
    deleteMax(m->alumnosHeap);
    masP = maxH(m->alumnosHeap);

   }
   destroyS(m->hechizos);
   destroyM(m->alumnos);
   deleteMax(m->alumnosHeap);
}


