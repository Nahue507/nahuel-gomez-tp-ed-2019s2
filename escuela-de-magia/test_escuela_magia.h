#ifndef TEST_ESC_MAGIA
#define TEST_ESC_MAGIA

#include "escuela_magia.h"
#include "../common/Test.h"


/***************************** Tests de escuela de magia *****************************/

TEST(test_fundarEscuela_escuelaVacia, {
   EscuelaDeMagia e = fundarEscuela();
	ASSERT_EQ(estaVacia(e), true);
})



TEST(test_inscribir_1_mago_not_estaVacia, {
   EscuelaDeMagia e = fundarEscuela();
   registrar("Merlin",e);
	ASSERT_EQ(estaVacia(e), false);
})



TEST(test_escuelaConUnMago_egresado, {
   EscuelaDeMagia e = fundarEscuela();
   Mago m = crearMago("Merlin");
   registrar("Merlin",e);
   Hechizo h = crearHechizo("expecto patronum", 80);
   enseniar(h,"Merlin",e);
	ASSERT_EQ(mismoMago(unEgresado(e),m), true);
})


TEST(test_escuelaConMasDeUn_egresado, {
   EscuelaDeMagia e = fundarEscuela();
   Mago m = crearMago("Merlin");
   Mago morg = crearMago("Morgana");
   registrar("Merlin",e);
   registrar("Morgana",e);
   Hechizo h = crearHechizo("expecto patronum", 80);
   Hechizo lumus = crearHechizo("lumus",2);
   Hechizo protegoTotarum = crearHechizo("protego totarum",98);
   enseniar(h,"Merlin",e);
   enseniar(lumus,"Merlin",e);
   enseniar(h,"Morgana",e);
   enseniar(lumus,"Morgana",e);
   enseniar(protegoTotarum,"Merlin",e);

	ASSERT_EQ(mismoMago(unEgresado(e),m), true);
})

TEST(test_quitarEgresado_esc_2_egresados_egresadoNoEsta, {
    EscuelaDeMagia e = fundarEscuela();
   Mago m = crearMago("Merlin");
   Mago morg = crearMago("Morgana");
   registrar("Merlin",e);
   registrar("Morgana",e);
   Hechizo h = crearHechizo("expecto patronum", 80);
   Hechizo lumus = crearHechizo("lumus",2);
   enseniar(h,"Merlin",e);
   enseniar(h,"Morgana",e);
   enseniar(lumus,"Morgana",e);
   quitarEgresado(e);
   ASSERT_EQ(mismoMago(unEgresado(e),morg), false);
})

/*
TEST(test_insertH_90_checkHeap, {
   MaxHeap h = emptyH();
   for(int i = 0; i < 90; i++) {
      Mago m = crearMago("harry");
      aprenderN(m, i);
      insertH(m, h);
      ASSERT_EQ(checkHeap(h), true);
      ASSERT_EQ(maxH(h) == m, true);
   }
})

TEST(test_deleteMax_1000_equal_elems_sizeH, {
   MaxHeap h = emptyH();
   Mago m = crearMago("harry");
   for(int i = 0; i < 1000; i++) {
      insertH(m, h);
   }
   for(int i = 999; i >= 0; i--) {
      deleteMax(h);
      ASSERT_EQ(sizeH(h) == i, true);
   }
})

TEST(test_deleteMax_90_checkHeap, {
   MaxHeap h = emptyH();
   Mago magos[90];

   for(int i = 0; i < 90; i++) {
      Mago m = crearMago("harry");
      aprenderN(m, i);
      magos[i] = m;
      insertH(m, h);
   }
   for(int i = 0; i < 90; i++) {
      ASSERT_EQ(maxH(h) == magos[89-i], true);
      deleteMax(h);
      ASSERT_EQ(checkHeap(h), true);
   }
})

TEST(test_destroyH, {
   MaxHeap h = emptyH();
   for(int i = 0; i < 90; i++) {
      Mago m = crearMago("harry");
      insertH(m, h);
   }
   destroyH(h);
})

*/

void correrTestEscuelaMagia() {
   test_fundarEscuela_escuelaVacia();
   test_inscribir_1_mago_not_estaVacia();
   test_escuelaConUnMago_egresado();
   test_quitarEgresado_esc_2_egresados_egresadoNoEsta();
}

#endif
